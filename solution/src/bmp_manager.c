#include "bmp_manager.h"
#include "bmp_structure.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

uint64_t read_header(FILE* file, struct BmpHeader* header) {
    return fread(header, 1, BMP_HEADER_SIZE, file);
}

void cleanup(uint8_t** bitmap, int already_allocated, uint8_t* padding_buffer) {
    for (int j = 0; j < already_allocated; j++) {
        free(bitmap[j]);
    }
    if (padding_buffer != NULL) {
        free(padding_buffer);
    }
    free(bitmap);
}

uint8_t** read_bitmap(FILE* file, size_t height, size_t length) {
    uint8_t** bitmap = malloc(sizeof(uint8_t*) * height);
    if (bitmap == NULL) {
        return NULL;
    }
    size_t padding_length = 0;
    uint8_t* padding_buffer = NULL;
    if (length % 4 != 0) {
        padding_length = 4 - (length % 4);
        padding_buffer = malloc(padding_length);
        if (padding_buffer == NULL) {
            free(bitmap);
            return NULL;
        }
    }
    for (int i = 0; i < height; ++i) {
        bitmap[i] = malloc(length);
        if (bitmap[i] == NULL) {
            cleanup(bitmap, i, padding_buffer);
            return NULL;
        }
        if (fread(bitmap[i], 1, length, file) < length) {
            cleanup(bitmap, i, padding_buffer);
            return NULL;
        }
        if (padding_buffer != NULL) {
            if (fread(padding_buffer, 1, padding_length, file) < padding_length) { // read padding to skip it
                cleanup(bitmap, i, padding_buffer);
                return NULL;
            }
        }
    }
    if (padding_buffer != NULL) {
        free(padding_buffer);
    }
    return bitmap;
}

_Bool read_bmp(FILE* file, struct BMP *bmp) {
     if (read_header(file, &bmp->bmp_header) < BMP_HEADER_SIZE) {
         fprintf(stderr, "Error occured when reading header\n");
         return false;
     }

    // read color table if it presents
    if (bmp->bmp_header.bOffBits != BMP_HEADER_SIZE) {
        uint64_t color_table_size = bmp->bmp_header.bOffBits - BMP_HEADER_SIZE;
        bmp->color_table = malloc(color_table_size);
        if (fread(bmp->color_table, 1, color_table_size, file) < color_table_size) {
            fprintf(stderr, "Error occured when reading color table.\n");
            return false;
        }
    } else {
        bmp->color_table = NULL;
    }
    // reading image, where byte-length equals color_byte * width in pixels
    bmp->image = read_bitmap(file, bmp->bmp_header.biHeight,
                            ((size_t) bmp->bmp_header.biBitCount / 8) * (size_t) bmp->bmp_header.biWidth);
    return bmp->image != NULL;
}

_Bool write_header(FILE* file, const struct BmpHeader* header) {
    return fwrite(header, BMP_HEADER_SIZE, 1, file) == 1;
}

_Bool write_bitmap(FILE* file, const uint8_t** data, size_t height, size_t width) {
    size_t padding_length = 0;
    uint8_t* padding = NULL; // preparing padding
    if (width % 4 != 0) {
        padding_length = 4 - width % 4;
        padding = malloc(padding_length);
        if (padding == NULL) {
            return false;
        }
        for (int i = 0; i < padding_length; ++i) {
            padding[i] = 0;
        }
    }
    for (size_t i = 0; i < height; ++i) {
        if (fwrite(data[i], 1, width, file) < width) {
            free(padding);
            return false;
        }
        if (padding_length != 0) {
            if (fwrite(padding, 1, padding_length, file) < padding_length) {
                free(padding);
                return false;
            };
        }
    }
    if (padding != NULL) {
        free(padding);
    }
    return true;
}

_Bool write_bmp(FILE* file, const struct BMP* image) {
    if (!write_header(file, &(image->bmp_header))) {
        return false;
    }
    if (image->color_table != NULL) {
        if (fwrite(image->color_table, 1, image->bmp_header.bOffBits - BMP_HEADER_SIZE, file) < image->bmp_header.bOffBits - BMP_HEADER_SIZE) {
            return false;
        }
    }
    return write_bitmap(file, (const uint8_t**) image->image, image->bmp_header.biHeight,
                 image->bmp_header.biWidth * (image->bmp_header.biBitCount / 8));
}
