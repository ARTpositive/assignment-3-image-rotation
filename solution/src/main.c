#include "image_rotation.h"

#include <stdbool.h>

#include <stdio.h>
#include <stdlib.h>

bool is_angle_correct(int angle) {
    return angle % 90 == 0;
}

int main(int argc, char **argv)
{
    if (argc != 4) {
        fprintf(stderr, "Invalid amount of arguments: %i", argc);
        return -1;
    }
    FILE *input = fopen(argv[1], "rb");
    if (input == NULL) {
        perror("Error");
        return -1;
    }
    FILE *output = fopen(argv[2], "wb");
    if (output == NULL) {
        perror("Error");
        return -1;
    }
    char *end; // for strol
    int angle = (int)strtol(argv[3], &end, 10);
    if (!is_angle_correct(angle)) {
        fprintf(stderr, "Angle incorrect. Only available is [0, +-90, +-180, +-270]");
    }

    if (!rotate_image(input, output, angle)) {
        fprintf(stderr, "Error has occured when reading/writing BMP.\n");
    }

    fclose(input);
    fclose(output);
    return 0;
}
