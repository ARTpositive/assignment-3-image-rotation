#include "image_rotation.h"
#include "bmp_manager.h"

#include "bmp_structure.h"
#include "malloc.h"
#include "string.h"
#include <stdbool.h>
#include <stdio.h>

_Bool get_rotated(FILE* input, struct BMP* bmp, int angle) {
    struct BMP base_image;
   if (!read_bmp(input, &base_image)) {
       fprintf(stderr, "Error: reading bmp failed.\n");
       return false;
   }
    if (angle == 0) {
        *bmp = base_image;
        return true;
    }

    // coping color table if it exists
    if (base_image.color_table != NULL) {
        bmp->color_table = malloc(base_image.bmp_header.bOffBits - BMP_HEADER_SIZE);
        for (uint32_t i = 0; i < base_image.bmp_header.bOffBits - BMP_HEADER_SIZE; ++i) {
            bmp->color_table[i] = base_image.color_table[i];
        }
    } else {
        bmp->color_table = NULL;
    }

    // coping header
    bmp->bmp_header = base_image.bmp_header;

    // swap width and height
    switch (angle) {
        case 90:
        case -90:
        case 270:
        case -270:
            bmp->bmp_header.biWidth = base_image.bmp_header.biHeight;
            bmp->bmp_header.biHeight = base_image.bmp_header.biWidth;
            break;
        case 180:
        case -180:
            bmp->bmp_header.biHeight = base_image.bmp_header.biHeight;
            bmp->bmp_header.biWidth = base_image.bmp_header.biWidth;
            break;
    }

    // necessary constants
    const size_t color_size = ((size_t) bmp->bmp_header.biBitCount / 8);
    const size_t rotated_bytes_length = (size_t) bmp->bmp_header.biWidth * color_size;

    // allocating memory for bitmap
    bmp->image = malloc(sizeof(uint8_t*) * bmp->bmp_header.biHeight);
    for (uint32_t i = 0; i < bmp->bmp_header.biHeight; ++i) {
        bmp->image[i] = malloc(rotated_bytes_length);
    }

    const size_t base_bytes_length = (size_t) base_image.bmp_header.biWidth * color_size;
    for (size_t i = 0; i < (size_t) base_image.bmp_header.biHeight; ++i) {
        for (size_t j = 0; j < base_bytes_length; j += color_size) {
            // every pixel has color_size byte length, so we are going to copy byte by byte
            for (size_t k = 0; k < color_size; ++k) {
                // j is index of current byte in current base-line
                // In other words j / color_size is index of current pixel

                // i is index of current line in base-image
                // then it would be i * color_size + k byte in rotated-image column
                // Because bitmap in file filling up from down to top, then we are use reverse formula
                // Out current_width = rotated_bytes_length - color_size * (i + 1) + k
                switch (angle) {
                    case -90:
                    case 270:
                        {
                            const size_t current_height = j / color_size;
                            const size_t current_width = rotated_bytes_length - i * color_size - color_size + k;
                            bmp->image[current_height][current_width] = base_image.image[i][j + k];
                            break;
                        }
                    case 90:
                    case -270:
                        {
                            const size_t current_height = bmp->bmp_header.biHeight - j / color_size - 1;
                            const size_t current_width = i * color_size + k;
                            bmp->image[current_height][current_width] = base_image.image[i][j + k];
                            break;
                        }
                    case -180:
                    case 180:
                        {
                            const size_t current_height = (bmp->bmp_header.biHeight - i - 1);
                            const size_t current_width = rotated_bytes_length - j - color_size + k;
                            bmp->image[current_height][current_width] = base_image.image[i][j + k];
                            break;
                        }
                }
            }
        }
    }

    size_t padding_length = 0; // if length divides by 4, then padding length is 0, otherwise 4 - length % 4
    if (bmp->bmp_header.biWidth * color_size % 4 != 0) {
        padding_length = 4 - (bmp->bmp_header.biWidth * color_size % 4);
    }
    const size_t rotated_bytes_length_with_padding = bmp->bmp_header.biWidth * color_size + padding_length;

    bmp->bmp_header.biSizeImage =
            bmp->bmp_header.biHeight * (uint32_t) rotated_bytes_length_with_padding;
    bmp->bmp_header.bfileSize = bmp->bmp_header.bOffBits + bmp->bmp_header.biSizeImage;

    // free all heap allocated data for base_image
    for (uint32_t i = 0; i < base_image.bmp_header.biHeight; ++i) {
        free(base_image.image[i]);
    }
    free(base_image.image);
    if (base_image.color_table != NULL) {
        free(base_image.color_table);
    }

    return bmp;
}

_Bool rotate_image(FILE* input, FILE* output, int angle) {
    struct BMP rotated_image;
    if (!get_rotated(input, &rotated_image, angle)) {
        return false;
    }
    if (!write_bmp(output, &rotated_image)) {
        fprintf(stderr, "Error occured when writing BMP to output file.\n");
    }

    // free all heap allocated data for bitmap
    for (uint32_t i = 0; i < rotated_image.bmp_header.biHeight; ++i) {
        free(rotated_image.image[i]);
    }
    free(rotated_image.image);
    if (rotated_image.color_table != NULL) {
        free(rotated_image.color_table);
    }
    return true;
}
