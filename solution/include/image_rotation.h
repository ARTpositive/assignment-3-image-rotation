#pragma once

#include "bmp_structure.h"

_Bool get_rotated(FILE* input, struct BMP* bmp, int angle);

_Bool rotate_image(FILE* input, FILE* output, int angle);
