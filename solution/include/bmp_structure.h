#pragma once

#include <stdint.h>
#include <stdio.h>

#define BMP_HEADER_SIZE 54


// packing to avoid alignment
#pragma pack(push, 1)
struct BmpHeader {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)
// redo packing to avoid troubles with other structures

struct BMP {
    struct BmpHeader bmp_header;
    uint8_t** image;
    uint8_t* color_table;
};
