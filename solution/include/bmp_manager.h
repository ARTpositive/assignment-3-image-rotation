#pragma once

#include "bmp_structure.h"
#include <stdint.h>

uint64_t read_header(FILE* file, struct BmpHeader* header);

uint8_t** read_bitmap(FILE* file, size_t height, size_t length);

_Bool read_bmp(FILE* file, struct BMP *bmp);

_Bool write_header(FILE* file, const struct BmpHeader* header);

_Bool write_bitmap(FILE* file, const uint8_t** data, size_t height, size_t width);

_Bool write_bmp(FILE* file, const struct BMP* image);
